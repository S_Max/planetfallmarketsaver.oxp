/* global missionVariables log*/

this.name = "PlanetFallMarketSaver";
this.author = "SMax";
this.copyright = "2016 SMax";
this.licence = "CC-BY-NC-SA 4.0";
this.description = "Save market in PlanetFall stations";
this.version = "0.1";

"use strict";

this._DEBUG = false;

this._MARKET = {};
this._PLANET = null;

this.shipExitedWitchspace = function() {
	this._logger("shipExitedWitchspace");
	this._MARKET = {};
};

this.startUpComplete = function() {
	this._MARKET = {};
	var t = missionVariables.PlanetFallMarketSaver_DATA;
	this._logger("startUpComplete: " + t);
	if (t) {
		this._MARKET = JSON.parse(t);
	}
};

this.playerWillSaveGame = function() {
	missionVariables.PlanetFallMarketSaver_DATA = JSON.stringify(this._MARKET);
	this._logger("playerWillSaveGame: " + missionVariables.PlanetFallMarketSaver_DATA);
};

this.shipApproachingPlanetSurface = function(planet) {
	this._logger("shipApproachingPlanetSurface: " + planet.name);
	this._PLANET = planet;
};

this.shipLeavingPlanetSurface = function(planet) {
	this._logger("shipLeavingPlanetSurface: " + planet.name);
	this._PLANET = null;
};

this.shipDockedWithStation = function(station) {
	this._logger("shipDockedWithStation: " + station.primaryRole);
	if (this._PLANET) {
		var id = this.getID(station, this._PLANET);
		if (this._MARKET[id]) {
			var market = this._MARKET[id];
			for (var i = 0; i < market.length; i++) {
				var m = market[i];
				station.setMarketPrice(m.k, m.p);
				station.setMarketQuantity(m.k, m.q);
			}
		}
	}
};

this.shipWillLaunchFromStation = function(station) {
	this._logger("shipWillLaunchFromStation: " + station.primaryRole);
	if (this._PLANET) {
		var id = this.getID(station, this._PLANET);
		var market = station.market;
		var res = [];
		for (var k in market) {
			var i = {
				k: k,
				q: market[k].quantity,
				p: market[k].price
			};
			res.push(i);
		}
		this._MARKET[id] = res;
	}
};

this._logger = function(msg) {
	if (this._DEBUG) {
		log(this.name, msg);
	}
};

this.getID = function(station, planet) {
	var id = {
		n: planet.name,
		r: station.primaryRole
	};
	return JSON.stringify(id);
};